# External pipelines for public projects

## Usage

For each new project, create a new branch. This will make it easier to follow
projects you're interested in even if they use the same definitions.

Copy over the `.gitlab-ci.yml` file from any branch. Change the `tree` URL to
point to a different tree from [pipeline definition's tree collection] if needed.


[pipeline definition's tree collection]: https://gitlab.com/cki-project/pipeline-definition/-/tree/main/trees
